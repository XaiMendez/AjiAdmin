app.config(['$routeProvider', function ($routeProvider) {
    
    $routeProvider
    .when('/', {
      templateUrl: 'index.html',
      controller: 'MainCtrl'
    })
    .when('/admin', {
      templateUrl: '../admin/index.html'
      //controller: 'MainCtrl'
    })
    .otherwise({redirectTo: '/'});

  }]);
