app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
            .when('/', {
              templateUrl: 'pages/options.html'

            })
            .when('/empresas-ingresar', {
              templateUrl: 'empresas/form.html'

            })
            .when('/empresas-listado', {
              templateUrl: 'empresas/list.html'

            })
            .when('/sitios-ingresar', {
              templateUrl: 'sitios/form.html'

            })
            .when('/sitios-nuevo', {
              templateUrl: 'sitios/form.html'

            })
            .when('/auditor', {
              templateUrl: 'auditor/options.html'

            })
            .when('/auditor-equipos-new', {
              templateUrl: 'auditor/equipos/form.html'

            })
            .when('/aire-comprimido-new', {
              templateUrl: 'auditor/aire-comprimido/form.html'

            })
            .when('/clima-cogene-new', {
              templateUrl: 'auditor/clima-cogene/form.html'

            })
            .when('/analisis-cogene-new', {
              templateUrl: 'auditor/analisis-cogene/form.html'

            })
            .when('/transporte-viaje-new', {
              templateUrl: 'auditor/transporte-viajes/form.html'

            })
             .when('/consumo-agua', {
              templateUrl: 'auditor/consumo-agua/form.html'

            })
            //.when('/list-data', {
            //
            //templateUrl: 'queries/allDataList.html',
            //controller: 'IngresosCtrl'
            //})
            .otherwise({redirectTo: '/'});

  }]);
